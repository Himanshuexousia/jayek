<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;

use Validator;

use Log;

use App\Model\Driver;

use App\Model\Notifications;


class DriverLogin extends Controller
{
	
	public static $checkDriver=array('phoneNumber'=>'required|exists:drivers,phoneNumber','loginPlatform'=>'required|in:0,1','notificationToken'=>'required');

   public function checkDriver()
   {
	   $all=Input::all();
	   $rules=DriverLogin::$checkDriver;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$authenticationToken=sha1('exousia'.uniqid());
			$sessionTime=sha1(date('Y-m-d H:i:s'));
			$user=Driver::getDriverData($all['phoneNumber']);
			$password=Hash::check($all['password'],$user->password);
			if($password)
			{
			    $get=Driver::updateDriverLoginData($all['phoneNumber'],$sessionTime,$authenticationToken,$all['loginPlatform'],$all['notificationToken']);
		        $data=Driver::getDriverData($all['phoneNumber']);
			    $json=array('driverId'=>$data->id,
				'phoneNumber'=>$data->phoneNumber,
				'firstName'=>$data->firstName,
				'lastName'=>$data->lastName,
				'email'=>$data->email,
				'sessionTime'=>$data->sessionTime,
				'authorizationToken'=>$data->authorizationToken,
				'loginPlatform'=>$data->loginPlatform,
				'deactivate'=>$data->deactivate);
			    $message=array('message'=>'Logged in successfully','type'=>'loginDriver');
			    Notifications::sendAndroidNotifications($message,$token=$all['notificationToken']);
			    return Response::json(['status'=>'1','message'=>'Driver Data','response'=>$json],200);
			}
			else
			{
				return Response::json(['status'=>'2','message'=>'WrongPassword','response'=>(object)array()],200);
			}
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
   }
	


}

?>
