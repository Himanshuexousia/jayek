<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio\Twiml;

use Twilio;

use Illuminate\Support\Facades\Input;

use Validator;

use Log;

class Webservice extends Controller
{
	
	public function slider()
	{
		$data=DB::table('slider')->get();
		return Response::json(['status'=>'1','message'=>"Slider Data",'response'=>$data]);
	}
	
	
	
	public function checkPhoneNumber()
	{
		$all=Input::all();
		$phone=$all['phone'];
		
		if(DB::table('users')->where('phoneNumber',$phone)->where('deactivate','0')->where('isDriver','0')->count()==1)
		{
			return Response::json(['status'=>'1','message'=>"User",'response'=>(object)array()]);
		}
		else if(DB::table('drivers')->where('phoneNumber',$phone)->where('deactivate','0')->where('isUser','0')->count()==1)
		{
			return Response::json(['status'=>'2','message'=>"Driver",'response'=>(object)array()]);
		}	
		else if(DB::table('users')->where('phoneNumber',$phone)->where('deactivate','0')->where('isDriver','0')->count()==1 && DB::table('drivers')->where('phoneNumber',$phone)->where('isUser','1')->count()==1)
		{
			return Response::json(['status'=>'3','message'=>"Is user and driver",'response'=>(object)array()]);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>"Register First",'response'=>(object)array()]);
		}
	}
	
	
	public function otp(Request $request)
	{
	    $mobile = $request->input('phone');
		$otp 	= $request->input('otp');
		$countryCode = $request->input('countryCode');
		$client = new Services_Twilio('AC2d33b9dfdf71f2987d4541a0dbb4a2c0', '9812ac0f92962b4665adf79d2477e3f4');
        try
		{
		    // Use the Twilio REST API client to send a text message
			$m = $client->account->messages->sendMessage('+1 765-626-3110', // the text will be sent from your Twilio number
			$countryCode.$mobile, // the phone number the text will be sent to
			"Your one time password is ".$otp // the body of the text message
					);
		} 
		catch(\Services_Twilio_RestException $e)
		{
		    // Return and render the exception object, or handle the error as needed
			return Response::json(array('status'=>'0','message'=>$e->getMessage(),'response'=>(object)array()));
        };
			return Response::json(array('status'=>'1','message'=>'otp sent','response'=>(object)array()),200);
	 }	
	
	public function truckType()
	{
		$data=DB::table('truckType')->get();
		return Response::json(['status'=>'1','message'=>"Truck Data",'response'=>$data]);
	}
	
	
	public function versionType()
	{
		$data="Version 1.0";
		return Response::json(['status'=>'1','message'=>'Version data','response'=>$data]);
	}
	
	public function demo()
	{

// $init = 150;
// $hours = floor($init / 3600);
// $minutes = floor(($init / 60) % 60);
// $seconds = $init % 60;

// echo "$hours:$minutes:$seconds";


$authorization=sha1('exousia'.uniqid());
 $sessionTime=sha1(date('Y-m-d H:i:s'));
$cretedAt=time();
$updatedAt=time();
return array('au'=>$authorization,'sess'=>$sessionTime,'cr'=>$cretedAt=time(),'up'=>$updatedAt);
	}
	
	// public function calculateFare(Request $request)
	// {
		// $type=$request->Input('typeId');
		// $lat1='28.55553279999999';
		// $lang1='77.233732';
		// $lat2='30.7293596';
		// $lang2='76.8407599';
		// $theta=$lang1 - $lang2;
           // $dist=sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
           // $dist=acos($dist);
           // $dist=rad2deg($dist);
           // $miles=$dist * 60 * 1.1515;
           // $dist=$miles * 1.609344; 
		   // $f=DB::table('truckType')->select('fare')->where('id',$type)->first();
		   // dd($f);
		   // $totalFare=$dist*intval($f->fare);
		   // return Response::json(['status'=>'1','message'=>'FareData','response'=>$totalFare]);
		   
	// }
	
	}

?>