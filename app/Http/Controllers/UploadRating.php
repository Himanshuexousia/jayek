<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;

use Validator;

use Log;

use App\Model\User;

use App\Model\Driver;

use App\Model\DriverRequest;

use App\Model\RequestData;

use App\Model\Destination;

use App\Model\Fare;

use App\Model\Notifications;

use App\Model\Calculation;

use App\Model\DriverRatings;

use App\Model\UserRatings;


class UploadRating extends Controller
{
	public static $uploadRating=array('userId'=>'required|exists:users,id',
	'driverId' =>'required|exists:drivers,id',
	'ratings' =>'required',
	'reviews',
	'isDriver' => 'required|in:0,1');
	 
	 public function uploadRatings()
	 {
		 $all=Input::all();
		$rules=UploadRating::$uploadRating;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			if($all['isDriver']=='1')
			{
				$user=new UserRatings();
			    $user->userId=$all['userId'];
				$user->driverId=$all['driverId'];
				$user->rating=$all['ratings'];
				$user->review=$all['reviews'];
			    $user->status='1';
			    $user->createdAt=time();
			    $user->updatedAt=time();
			    $user->save();
			}
			else
			{
				$driver=new DriverRatings();
			    $driver->userId=$all['userId'];
				$driver->driverId=$all['driverId'];
				$driver->ratings=$all['ratings'];
				$driver->reviews=$all['reviews'];
			    $driver->status='1';
			    $driver->createdAt=time();
			    $driver->updatedAt=time();
			    $driver->save();
			}
			return Response::json(['status'=>'1','message'=>'Ratings','response'=>(object)array()],200);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
	 }
	
}

?>