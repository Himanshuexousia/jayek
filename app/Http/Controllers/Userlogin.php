<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;


use Validator;

use Log;

use App\Model\User;

use App\Model\Notifications;

use App\Model\RequestData;


class Userlogin extends Controller
{
	
   public static $checkUser=array('phoneNumber'=>'required','loginPlatform'=>'required|in:0,1','notificationToken'=>'required');

   public function checkUser()
   {
	   $all=Input::all();
	   $rules=Userlogin::$checkUser;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
            $user=DB::table('users')->where('phoneNumber',$all['phoneNumber'])->first();
            if(count($user)!=0 || $user!=null)
            {
			$authenticationToken=sha1('exousia'.uniqid());
			$sessionTime=sha1(date('Y-m-d H:i:s'));
			$get=User::updateLoginData($all['phoneNumber'],$sessionTime,$authenticationToken,$all['loginPlatform'],$all['notificationToken']);
			$data=User::getData($all['phoneNumber']);
			$json=array('userId'=>$data->id,
			'phoneNumber'=>$data->phoneNumber,
			'firstName'=>$data->firstName,
			'lastName'=>$data->lastName,
			'email'=>$data->email,
			'sessionTime'=>$data->sessionTime,
			'authenticationToken'=>$data->authenticationToken,
			'loginPlatform'=>$data->loginPlatform,
			'deactivate'=>$data->deactivate);
			
			$message=array('message'=>'Logged in successfully','type'=>'loginUser');
			Notifications::sendAndroidNotifications($message,$token=$all['notificationToken']);
			
			return Response::json(['status'=>'1','message'=>'User Exists','response'=>$json],200);
		}
		else
		{
			return Response::json(['status'=>'2','message'=>'User Does not exist','response'=>(object)array()],200);
		}
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
   }
   
   public static $userRides=array('userId'=>'required|exists:users,id');
   public function userRides()
   {
	   $all=Input::all();
	   $rules=Userlogin::$userRides;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=RequestData::requestUserData($all['userId']);
			if($data!='0')
			{
				return Response::json(['status'=>'1','message'=>'Rides Data','response'=>$data],200);
			}
			else
			{
				return Response::json(['status'=>'2','message'=>'No Rides','response'=>array()],200);
			}
			
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>array()],200);
		}
   }
	


}

?>
