<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;

use Validator;

use Log;

use App\Model\Register;

use App\Model\User;

use App\Model\Calculation;


class Calculatefare extends Controller
{
	
	
 public static $fare=array('truckId'=>'required|exists:truckType,id',
	 'sourceLat'=>'required',
	 'sourceLang'=>'required',
	 'dropLat'=>'required',
	 'dropLang'=>'required');
	 
      public function calculateFare()
	  {
		$all=Input::all();
		$rules=Calculatefare::$fare;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$distance=calculation::calculateDistance($all['sourceLat'],$all['sourceLang'],$all['dropLat'],$all['dropLang']);
			$distance=round($distance,2);
			$data=Calculation::calculateFare($all['truckId'],$distance);
			return Response::json(['status'=>'1','message'=>'Fare calculated','response'=>array('distance'=>(string)$distance,'fare'=>(string)round($data,2))]);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
	  }
	  
	  
	}
