<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;

use Validator;

use Log;

use App\Model\User;

use App\Model\Driver;

use App\Model\DriverRequest;

use App\Model\RequestData;

use App\Model\Destination;

use App\Model\Fare;

use App\Model\Notifications;

use App\Model\Calculation;


class RequestGenerate extends Controller
{
	
	public static $generateRequest=array('userId'=>'required|exists:users,id',
	'paymentType'=>'required|in:0,1',
	'paymentStatus'=>'required|in:0,1',
    'souceAddress'=>'required',
    'sourceLatitude'=>'required',
    'sourceLongitude'=>'required',
    'dropAdress'=>'required',
    'dropLatitude'=>'required',
    'dropLongitude'=>'required',
    'truckType'=>'required',
    'distance'=>'required',
	'fareGenerated'=>'required'
	);
	public function generateRequest()
	{
		$all=Input::all();
		$rules=RequestGenerate::$generateRequest;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$driverId=RequestData::getRandomDriver($all['truckType'],$all['sourceLatitude'],$all['sourceLongitude'],$all['distance']);		
			$dId=$driverId!=0 ? $driverId : 0;
			if($dId!=0)
			{
			    $data=new Destination();  
			    $data->userId=$all['userId'];
			    $data->sourceLocation=$all['souceAddress'];
			    $data->sourceLatitude=$all['sourceLatitude'];
			    $data->sourceLongitude=$all['sourceLongitude'];
			    $data->dropLatitude=$all['dropLatitude'];
			    $data->dropLongitude=$all['dropLongitude'];
			    $data->dropLocation=$all['dropAdress'];
			    $data->status='1';
			    $data->createdAt=time();
			    $data->updatedAt=time();
			    $data->save();

			    $request=new DriverRequest();
			    $request->userId=$all['userId'];
			    $request->driverId=$dId;
			    $request->pickupStatus='0';
			    $request->paymentType=$all['paymentType'];
			    $request->paymentStatus=$all['paymentStatus'];
		        $request->createdAt=time();
			    $request->updatedAt=time();
			    $request->save();
			
			   $data=RequestData::updateRequestId($all['userId']);
		       if($data!='0')
			   {
			        $fare=new Fare();
			        $fare->requestId=$data;
				    $fare->userId=$all['userId'];
				    $fare->driverId=$dId;
				    $fare->fare=$all['fareGenerated'];
				    $fare->createdAt=time();
				    $fare->updatedAt=time();
				    $fare->save();
				    $requestId=Driver::getRequestId($all['userId']);
				    $user=Driver::getUserDataFromId($all['userId'],$requestId);
				    if($user!='0')
				    {
					$drive=Driver::getDriverDataFromId($dId,$requestId);
			
					if($drive!='0')
					{
					     $message=array('message'=>'Your pickup request has been made','type'=>'requestGenerateUser');
				         $message1=array('message'=>'You recieved a new pickup request','type'=>'requestGenerateDriver','riderFirstName'=>$user->firstName,'riderLastName'=>$user->lastName,'sourceLocation'=>$user->sourceLocation,'dropLocation'=>$user->dropLocation,'startTime'=>$user->createat,'phoneNumber'=>$user->phoneNumber);
			             Notifications::sendAndroidNotifications($message,$token=$user->notificationToken);
				         Notifications::sendAndroidNotifications($message1,$token=$drive->notificationToken);
					}
				}
				return Response::json(['status'=>'1','message'=>'Request Generated','response'=>array('requestId'=>$requestId)],200);
				
			}
			else
			{
				 return Response::json(['status'=>'2','message'=>'Driver not found','response'=>(object)array()],200);
			}
			
		   
			}
			else
			{
			   return Response::json(['status'=>'2','message'=>'Driver not found','response'=>(object)array()],200);
			}
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
	}
	
	
	public static $driverrequest=array('driverId'=>'required|exists:drivers,id|exists:requests,driverId');
	public function driverRequest()
	{
		$all=Input::all();
		$rules=RequestGenerate::$driverrequest;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=RequestData::requestDriverData($all['driverId']);
			if($data!='0')
			{
				return Response::json(['status'=>'1','message'=>'Driver Request Data','response'=>$data],200);
			}
			else
			{
				return Response::json(['status'=>'2','message'=>'No request','response'=>array()],200);
			}
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>array()],200);
		}
	}
	
	
	public static $pickUp=array('driverId'=>'required|exists:drivers,id|exists:requests,driverId','pickupStatus'=>'required|in:0,1,2,3,4,5','requestId'=>'required|exists:requests,id|exists:userDestinationData,requestId|exists:fareGenerated,requestId');
	public function changePickupStatus()
	{
		$all=Input::all();
		$rules=RequestGenerate::$pickUp;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
		
			$end=RequestData::updateRequestStatus($all['requestId'],$all['pickupStatus']);
			if($end!=0)
			{
			    if($all['pickupStatus']=='4')
			    {
				   $change=RequestData::changeStatusOfRequest($all['requestId'],$all['pickupStatus']);
			    }
				else if($all['pickupStatus']=='3')
				{
					 $change2=RequestData::endTripTime($all['requestId']);
					 $daata=Driver::getUserFromRequestId($all['requestId']);
					 $tripData=RequestData::requestUserRequest($daata->id,$all['requestId']);
					 if($tripData!='0')
					 {
					     $message=array('message'=>'Ride Completed, you have reached your destination!','type'=>'completedRide','colorImage'=>$tripData->colorImage,'dropLocation'=>$tripData->dropLocation);
					     Notifications::sendAndroidNotifications($message,$token=$daata->notificationToken);
					 }
					
				}
				else if($all['pickupStatus']=='5')
				{
                    	 $change=RequestData::changeStatusOfRequest($all['requestId'],$all['pickupStatus']);	
		                 $data=Driver::getDriverDataFromId($all['driverId'],$all['requestId']); 
                         $message=array('message'=>'The trip has been cancelled by the user','type'=>'cancelRequest');
			             Notifications::sendAndroidNotifications($message,$token=$data->notificationToken);						 
				}
				else if($all['pickupStatus']=='1')
				{
					 $daata=Driver::getUserFromRequestId($all['requestId']); 
					 $driver=Driver::getDriverDataFromRequest($all['requestId']);
                     $startTrip=User::startTrip($all['requestId']);
					 $message=array('message'=>'Your Driver '. $driver->firstName .' '. $driver->lastName .' is arriving...','type'=>'acceptRequest');
			         Notifications::sendAndroidNotifications($message,$token=$daata->notificationToken);
				}
			    return Response::json(['status'=>'1','message'=>'Status Changed','response'=>array()],200);
			}
			else
			{
				return Response::json(['status'=>'0','message'=>'Error','response'=>array()],200);
			}
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>array()],200);
		}
	}

	public function driverHistory()
	{
		$all=Input::all();
		$rules=RequestGenerate::$driverrequest;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$end=RequestData::requestHistory($all['driverId']);
			if($end!='0')
			{
				return Response::json(['status'=>'1','message'=>'data','response'=>$end],200);
			}
			else
			{
				return Response::json(['status'=>'0','message'=>'No data','response'=>array()],200);
			}
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>array()],200);
		}
	}
	
	
	public static $fareSlipData=array('requestId'=>'required|exists:requests,id|exists:userDestinationData,requestId|exists:fareGenerated,requestId');
	public function fareSlipData()
	{
		$all=Input::all();
		$rules=RequestGenerate::$fareSlipData;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=Calculation::fareSlip($all['requestId']);
			return Response::json(['status'=>'1','message'=>'Fare Slip','response'=>$data],200);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
	}

	
	public static $driverDataOnRequest=array('userId'=>'required|exists:users,id','requestId'=>'required|exists:requests,id');
	public function driverData()
	{
		$all=Input::all();
		$rules=RequestGenerate::$driverDataOnRequest;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=Driver::getDriverDataFromRequest($all['requestId']);
			$rating=Calculation::getAverageRating($data->id);
			$json=array('driverFirstName'=>$data->firstName,
			'driverLastName'=>$data->lastName,
			'driverPhoneNumber'=>$data->phoneNumber,'vehicleNumber'=>$data->vehicleNumber,'truckTypeId'=>$data->truckTypeId,'currentLatitude'=>$data->latitude,'currentLongitude'=>$data->longitude,
			'driverId'=>intval($data->driverId),
			'driverRating'=>round($rating,1));
			return Response::json(['status'=>'1','message'=>'Driver Data','response'=>$json],200);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
	}
	
	
	public function driverLocation()
	{
		$all=Input::all();
		$rules=RequestGenerate::$driverDataOnRequest;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=Driver::getDriverDataFromRequest($all['requestId']);
			// dd($data);
			$json=array('currentLatitude'=>$data->latitude,'currentLongitude'=>$data->longitude,'oldLatitude'=>$data->oldLatitude,'oldLongitude'=>$data->oldLongitude,'changedStatus'=>$data->pickupStatus);
			return Response::json(['status'=>'1','message'=>'Driver Location Data','response'=>$json],200);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
	}

	
	
}

?>
