<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;


use Validator;

use Log;

use App\Model\User;

use App\Model\Driver;


class Nearby extends Controller
{
	
	public static $nearbyDriver=array(
	'truckType'=>'required|exists:truckType,id',
	'latitude'=>'required',
	'longitude'=>'required',
	'distance'=>'required');

    public function nearbyDrivers()
    {
	    $all=Input::all();
		$rules=Nearby::$nearbyDriver;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=Driver::nearbyData($all['truckType'],$all['latitude'],$all['longitude'],$all['distance']);
			if(count($data)>0)
			{
				return Response::json(['status'=>'1','message'=>'NearBy Drivers','response'=>$data]);
			}
			else
			{
				return Response::json(['status'=>'0','message'=>'No nearby Drivers','response'=>array()]);
		    }
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>array()]);
		}
    }

}

?>
