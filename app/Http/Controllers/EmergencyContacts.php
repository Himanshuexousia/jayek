<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;

use Validator;

use Log;

use App\Model\User;

use App\Model\Driver;

use App\Model\DriverRequest;

use App\Model\RequestData;

use App\Model\Destination;

use App\Model\Fare;

use App\Model\Notifications;

use App\Model\Calculation;

use App\Model\Emergency;


class EmergencyContacts extends Controller
{
	
	public static $emergencyContact=array('userId'=>'required|exists:users,id','contactNumber'=>'required','fullName'=>'required','priority'=>'required|in:1,2,3,4,5');
	public function emergencyContacts()
	{
		$all=Input::all();
		$rules=Self::$emergencyContact;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=User::getEmegencyContacts($all['userId']);
			if(count($data)<5 || $data=='0')
			{
			   $check=User::checkPriority($all['userId'],$all['priority'],$all['contactNumber']);
			   // dd($check);
			   if($check!='0')
			   {
				   return Response::json(['status'=>'3','message'=>'Unable to add the contact. Kindly check the phone number or the priority of the contact','response'=>(object)array()],200);
			   }
			   else
			   {
					$user = new Emergency();
					$user->userId=$all['userId'];
					$user->emergencyContact=$all['contactNumber'];
					$user->emergencyName=$all['fullName'];
					$user->priority=$all['priority'];
					$user->status='1';
					$user->createdAt=time();
					$user->updatedAt=time();
					$user->save();
					return Response::json(['status'=>'1','message'=>'Emergency Contact Added','response'=>(object)array()],200);
			   }
			}
			else
			{
				return Response::json(['status'=>'2','message'=>'You already have five contacts in the emergency contact list','response'=>(object)array()],200);
			}
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
	}

	public static $contactList=array('userId'=>'required|exists:users,id|exists:emergencyContacts,userId');
	public function getEmegencyContactsFull()
	{
		$all=Input::all();
		$rules=Self::$contactList;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$data=User::getEmegencyContacts($all['userId']);
			if($data!='0' && count($data)>0)
			{
				return Response::json(['status'=>'1','message'=>'Emergency Contact List','response'=>$data],200);
			}
			else
			{
				return Response::json(['status'=>'2','message'=>'No Emergency Contact','response'=>array()],200);
			}
	    }
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>array()],200);
		}
	}
	
	
}

?>
