<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;


use Validator;

use Log;

use App\Model\User;

use App\Model\Driver;

use App\Model\Register;

use App\Model\DriverRegister;


class EditProfile extends Controller
{
	
	public static $edit=array(
	'id'=>'required',
	'isDriver'=>'required|in:0,1',
	'email','fullName');

    public function editProfile()
    {
	    $all=Input::all();
		$rules=EditProfile::$edit;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			$name=explode(" ",$all['fullName']);
		         $firstName=!empty($name[0])? $name[0] : "";
		         $lastName=!empty($name[1])? $name[1] : "";
			if($all['isDriver']=='0')
			{
				 
			     $user = new Register();
                 $user->exists = true;
                 $user->id = $all['id']; //already exists in database.
                 $user->firstName = $firstName;
                 $user->lastName = $lastName;
                 $user->email = $all['email'];
                 $user->save();
			}
			else
			{
				  $driver = new DriverRegister();
				  $driver->exists=true;
				  $driver->id = $all['id'];
				  $driver->firstName = $firstName;
				  $driver->lastName = $lastName;
				  $driver->email = $all['email'];
				  $driver->save();
			}
			return Response::json(['status'=>'1','message'=>'Your Profile is updated successfully','response'=>array('firstName'=>$firstName,'lastName'=>$lastName,'email'=>$all['email'])]);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()]);
		}
    }
	
	public static $changePassword=array('id'=>'required','isDriver'=>'required|in:0,1','newPassword'=>'required','oldPassword'=>'required','confirmPassword'=>'required');
	public function changePassword()
	{
		$all=Input::all();
		$rules=EditProfile::$changePassword;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
			if($all['newPassword']==$all['confirmPassword'])
			{
				$userData=User::getUserDataFromId($all['id'],$all['isDriver']);
				if(Hash::check($all['oldPassword'],$userData->password))
			    {
						$pass2=Hash::make($all['newPassword']);
						if($all['isDriver']=='0')
			            {
				            $user = new Register();
                            $user->exists = true;
                            $user->id = $all['id']; //already exists in database.
                            $user->password = $pass2;
                            $user->save();
			           }
			           else
			           {
				            $driver = new DriverRegister();
				            $driver->exists=true;
				            $driver->id = $all['id'];
				            $driver->password = $pass2;
				            $driver->save();
			            }
						return Response::json(['status'=>'1','message'=>'password changed','response'=>(object)array()]);
				}
			    else
			    {
				    return Response::json(['status'=>'3','message'=>"old password doesn't match",'response'=>(object)array()]);
			    }
				
			}
			else
			{
				return Response::json(['status'=>'2','message'=>'New Password and Confirm Password do not match','response'=>(object)array()]);
			}
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()]);
		}
	}
	

}

?>
