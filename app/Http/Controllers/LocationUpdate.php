<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;

use Validator;

use Log;

use App\Model\Driver;

use App\Model\User;


class LocationUpdate extends Controller
{

    public static $updateDriverLocation=array(
	'driverId'=>'required|exists:drivers,id',
	'latitude'=>'required',
	'longitude'=>'required'
	);
	public function updateDriverLocation()
	{
		$all=Input::all();
		$rules=LocationUpdate::$updateDriverLocation;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
		    Driver::updateDriverLatLang($all['driverId'],$all['latitude'],$all['longitude']);
		    return Response::json(['status'=>'1','message'=>'Location Updated','response'=>array()],200);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>array()],200);
		}
	}
	
	
	
	


}

?>
