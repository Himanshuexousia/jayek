<?php

namespace App\Http\Controllers;
 
use illuminate\Http\Request;
 
use Illuminate\Support\Facades\Input;
 
use App\Http\Requests;
 
use DB;

use File;

use Hash;

use Auth;

use Response;

use Services_Twilio;

use Twilio;


use Validator;

use Log;

use App\Model\Register;

use App\Model\User;

use App\Model\Notifications;


class Userregister extends Controller
{

public static $register=array(
	'fullName'=>'required|max:50',
	'phoneNumber'=>'required|min:13|numeric|unique:users',
	'email',
	'loginPlatform'=>'required|in:0,1', // 0 is for android and 1 is for Ios
	'notificationToken'=>'required',
	'password'=>'required|min:8'
	);
	public function registerUser()
	{
		$all=Input::all();
		$rules=Userregister::$register;
		$validator=Validator::make($all,$rules);
		if($validator->passes())
		{
 
		    $name=explode(" ",$all['fullName']);
		    $firstName=!empty($name[0])? $name[0] : "";
		    $lastName=!empty($name[1])? $name[1] : "";
		    $authorization=sha1('exousia'.uniqid());
		    $sessionTime=sha1(date('Y-m-d H:i:s'));
		    $password=Hash::make($all['password']);
		    $time=time();
		
		    $user = new Register();
		    $user->phoneNumber=$all['phoneNumber'];
		    $user->firstName=$firstName;
		    $user->lastName=$lastName;
		    $user->userName=" ";
		    $user->email=$all['email'];
		    $user->password=$password;
		    $user->referCode=" ";
		    $user->notificationToken=$all['notificationToken'];
		    $user->sessionTime=$sessionTime;
		    $user->authenticationToken=$authorization;
		    $user->loginPlatform=$all['loginPlatform'];
		    $user->deactivate='0';
		    $user->status='1';
			$user->createdAt=time();
			$user->updatedAt=time();
		    $user->save();
		
		    $data=User::getData($all['phoneNumber']);
			
			$message=array('message'=>'Registered Successfully - Welcome to Jayek','type'=>'registerUser');
			Notifications::sendAndroidNotifications($message,$token=$all['notificationToken']);
		    return Response::json(['status'=>'1','message'=>'User Registered','response'=>$data],200);
		}
		else
		{
			return Response::json(['status'=>'0','message'=>$validator->getMessageBag()->first(),'response'=>(object)array()],200);
		}
	}
	


}

?>
