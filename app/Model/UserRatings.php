<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Hash;

class UserRatings extends Model
{
	 protected $table = 'userRatings';
	 protected $primaryKey = 'id';
	 public $timestamps = false;

	  protected $fillable = array(
       'userId',
	   'driverId',
	   'rating',
	   'review',
	   'status',
	   'createdAt',
	   'updatedAt'
    );
	
}