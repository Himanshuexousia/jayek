<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
	protected $table='users';
	public $timestamps=false;
	
	protected function sendAndroidNotifications($message,$token)
	{
		// $token="fKUpdztlFug:APA91bGrckeZC8yaZqYBht-Ra7VyPKN0dAdgmoSWsQAc-PUvvPFh2Ob5W4jgwtNnAY5VRB681hQTK11vZV_V9RIk_ab0aLT6Rq2o6sIpz32bOp6RozwQWScA_2Ol-L6lWUqyfZEYnDoQ";
		$url='https://fcm.googleapis.com/fcm/send';
		$fields=array('registration_ids'=>array($token),'data'=>array("message"=>$message));
		$fields=json_encode($fields);
        $headers=array('Authorization: key='."AIzaSyBFrJwZZ5X8yJHCeJuz2XtMhGELVvh0TmI",'Content-Type:application/json');
    	$ch=curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields);
	   	$result= curl_exec($ch);
	   	// dd($result);
		curl_close($ch);
		// dd(json_encode$ch);
		// return 1;
	}
	
	
	protected function sendIosNotification()
	{
		
 // Put your private key's passphrase here:
  	    // $passphrase = '123456';
		// $nottype='123';
 ////////////////////////////////////////////////////////////////////////////
 		// $ctx = stream_context_create();
 		// stream_context_set_option($ctx, 'ssl', 'local_cert','/var/www/vhosts/wuffiq.com/httpdocs/webs/WuffiqPushCertificate.pem');
 		// stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
 // dd($ctx);
 // Open a connection to the APNS server
 		// $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		// if (!$fp)
		// exit("Failed to connect: $err $errstr" . PHP_EOL);
		// echo 'Connected to APNS' . PHP_EOL;
		// ob_flush();
		// flush();
 // Encode the payload as JSON
        // $body['aps'] = array(
            // 'alert' => json_encode($message),
            // 'sound' => 'default',
            // 'badge' => '+1'
        // );

 		// $payload = json_encode($body);
 // Build the binary notification
 		// $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
	
 // Send it to the server
 		// stream_set_blocking ($fp,0);
 		// $result = fwrite($fp, $msg, strlen($msg));
 		// if (!$result)
 		// {
  		// echo json_encode(array('status'=>'0')) . PHP_EOL;
 		 // echo $fp;
 		// }
 		// else
 		// {
 		// echo json_encode(array('status'=>'1')) .PHP_EOL;
  		  // ob_flush();
  		  // flush();
 		// }
 		// self::checkAppleErrorResponse($fp);
 		// fclose($fp);

	}
}

