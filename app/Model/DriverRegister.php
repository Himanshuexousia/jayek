<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Hash;

class DriverRegister extends Model
{
	 protected $table = 'drivers';
	 protected $primaryKey = 'id';
	 public $timestamps = false;

	  protected $fillable = array(
        'phoneNumber',
        'firstName',
        'lastName',
		'username',
		'email',
		'password',
		'notificationToken',
		'sessionTime',
		'authenticationToken',
		'loginPlatform',
		'deactivate',
		'driverStatus',
		'isUser',
		'truckDataId',
		'status',
		'createdAt',
		'updatedAt'
    );
	
}
