<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
  
	protected $table='userDestinationData';
	protected $primaryKey = 'id';
	public $timestamps=false;
	
	protected $fillable = array(
       'userId',
	   'sourceLocation',
	   'sourceLatitude',
	   'sourceLongitude',
	   'dropLatitude',
	   'dropLongitude',
	   'dropLocation',
	   'status',
	   'createdAt',
	   'updatedAt'
    );
	
}
