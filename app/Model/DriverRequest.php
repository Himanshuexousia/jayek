<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class DriverRequest extends Model
{
  
	protected $table='requests';
	protected $primaryKey = 'id';
	public $timestamps=false;
	
	protected $fillable = array(
       'userId',
	   'driverId',
	   'pickupStatus',
	   'paymentType',
	   'paymentStatus',
	   'status',
	   'createdAt',
	   'updatedAt'
    );
	
}
