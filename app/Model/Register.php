<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Hash;

class Register extends Model
{
	 protected $table = 'users';
	 protected $primaryKey = 'id';
	 public $timestamps = false;

	  protected $fillable = array(
        'phoneNumber',
        'firstName',
        'lastName',
		'username',
		'email',
		'password',
		'referCode',
		'notificationToken',
		'sessionTime',
		'authenticationToken',
		'loginPlatform',
		'deactivate',
		'status',
		'createdAt',
		'updatedAt'
    );
	
}
