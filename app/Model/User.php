<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use DB;

class User extends Model
{
    //
	protected $table = 'users';
	public $timestamps = false;
	
	
     protected function getData($phone)
	 {
		 $get=User::where('phoneNumber','=',$phone)->first();
		 return $get;
	 }
	
	protected function updateLoginData($phone,$sessionTime,$authenticationToken,$loginPlatform,$notificationToken)
	{
		$update=User::where('phoneNumber',$phone)->update(['sessionTime'=>$sessionTime,'authenticationToken'=>$authenticationToken,'loginPlatform'=>$loginPlatform,"notificationToken"=>$notificationToken,"updatedAt"=>time()]);
		return 1;
	}
	
	protected function getLocation($userId,$requestId)
	{
		$requestData=DB::table('userDestinationData')->where('userId',$userId)->where('requestId',$requestId)->first();
		$data=count($requestData)>0 ? $requestData : 0 ;
		return $data;
	}
	protected function getUserDataFromId($userId,$isDriver)
	{
		if($isDriver=='0')
		{
		    $data=DB::table('users')->where('id',$userId)->first();
		}
		else
		{
			$data=DB::table('drivers')->where('id',$userId)->first();
		}
		return $data;
	}
	protected function startTrip($requestId)
	{
		$data=DB::table('userDestinationData')->where('requestId',$requestId)->update(['tripStartedAt'=>time()]);
		return 1;
	}
	protected function getEmegencyContacts($userId)
	{
		$data=DB::table('emergencyContacts')->where('userId',$userId)->get();
		if(count($data)>0)
		{
			return $data;
		}
		else
		{
			return '0';
		}
	}
	protected function checkPriority($userId,$priority,$phone)
	{
		$data=DB::table('emergencyContacts')->where([['userId','=',$userId],['priority','=',$priority]])->orWhere([['userId','=',$userId],['emergencyContact','=',$phone]])->get();
		// dd($data);
		if(count($data)>0)
		{
			return $data;
		}
		else
		{
			return '0';
		}
	}
}
