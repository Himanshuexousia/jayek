<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class RequestData extends Model
{
  
	protected $table='requests';
	protected $primaryKey = 'id';
	public $timestamps=false;

	protected function updateRequest($requestId,$pickupStatus,$paymentType,$paymentStatus)
	{
		$update=DB::table('requests')->where('id',$requestId)->update(['pickupStatus'=>$pickupStatus,'paymentType'=>$paymentType,'paymentStatus'=>$paymentStatus,'updatedAt'=>time()]);
		return 1;
	}
	
	protected function updateRequestId($userId)
	{
		$data=DB::table('requests')->where('userId',$userId)->where('pickupStatus','0')->orderBy('id','DESC')->first();
		if(count($data)==1) {
		$insert=DB::table('userDestinationData')->where('userId',$userId)->where('status','1')->where('id',$data->id)->update(['requestId'=>$data->id]);
		return $data->id;
		}
	    else
		{
			return 0;
		}
		
	}
	
	protected function requestDriverData($driverId)
	{
		$requestData=DB::table('requests')->select('requests.id as requestId','requests.driverId as driverId','requests.pickupStatus as pickupStatus','requests.paymentType as paymentType','requests.paymentStatus as paymentStatus','userDestinationData.*','users.id as userId','users.phoneNumber as phoneNumber','users.firstName as firstName','users.lastName as lastName')->where('requests.driverId',$driverId)->join('userDestinationData','userDestinationData.requestId','=','requests.id')->join('users','users.id','=','requests.userId')->where('requests.status','1')->orderBy('requests.id','DESC')->groupBy('requests.id')->get();
	    $data=count($requestData)>0 ? $requestData : '0';
		return $data;
	}

	protected function requestUserData($userId)
	{
		$requestData=DB::table('requests')->select('requests.id as requestId','requests.userId as userId','requests.pickupStatus as pickupStatus','requests.paymentType as paymentType','requests.paymentStatus as paymentStatus','requests.updatedAt as updatedAt','userDestinationData.dropLocation as dropLocation','drivers.id as driverId','drivers.phoneNumber as phoneNumber','drivers.firstName as firstName','drivers.lastName as lastName','truckData.vehicleNumber as vehicleNumber','truckType.truckType as truckType','truckType.colourImage as colorImage','fareGenerated.fare as fare')->where('requests.userId',$userId)->join('userDestinationData','userDestinationData.requestId','=','requests.id')->join('drivers','drivers.id','=','requests.driverId')->join('truckData','truckData.id','=','drivers.truckDataId')->join('truckType','truckType.id','=','truckData.truckTypeId')->join('fareGenerated','fareGenerated.requestId','=','requests.id')->where('requests.status','0')->orderBy('requests.id','DESC')->groupBy('requests.id')->get();
	    $data=count($requestData)>0 ? $requestData : '0';
		return $data;
	}
	
		protected function requestUserRequest($userId,$requestId)
	{
		$requestData=DB::table('requests')->select('requests.id as requestId','requests.userId as userId','requests.pickupStatus as pickupStatus','requests.paymentType as paymentType','requests.paymentStatus as paymentStatus','requests.updatedAt as updatedAt','userDestinationData.dropLocation as dropLocation','drivers.id as driverId','drivers.phoneNumber as phoneNumber','drivers.firstName as firstName','drivers.lastName as lastName','truckData.vehicleNumber as vehicleNumber','truckType.truckType as truckType','truckType.colourImage as colorImage','fareGenerated.fare as fare','requests.status as status')->where('requests.userId',$userId)->join('userDestinationData','userDestinationData.requestId','=','requests.id')->join('drivers','drivers.id','=','requests.driverId')->join('truckData','truckData.id','=','drivers.truckDataId')->join('truckType','truckType.id','=','truckData.truckTypeId')->join('fareGenerated','fareGenerated.requestId','=','requests.id')->where('requests.status','1')->where('requests.pickupStatus','!=','5')->where('requests.id',$requestId)->first();
	    $data=count($requestData)>0 ? $requestData : '0';
		return $data;
	}
	
	protected function confirmedByUser($userId,$requestId)
	{
		$requestData=DB::table('requests')->where('userId',$userId)->where('id',$requestId)->where('pickupStatus','0')->first();
		$data=count($requestData)>0 ? $requestData : '0';
		return $data;
	}
	
	protected function requestData($requestId,$pickupStatus)
	{
		$requestData=DB::table('requests')->where('id',$requestId)->where('pickupStatus',$pickupStatus)->first();
		$data=count($requestData)>0 ? $requestData : '0';
		return $data;
	}
	
	protected function getRandomDriver($truckType,$latitude,$longitude,$distance)
	{
		 $requestData=Driver::nearbyData($truckType,$latitude,$longitude,$distance);
		
		 if(count($requestData)!=0)
		 {
		 $sss=array_rand($requestData);
		 $id=$requestData[$sss];
		 $dat=count($id)==1 ? $id->driverId : 0;
		}
		else
		{
			$dat=0;
		}
		 return $dat;
    }
	
	protected function changeStatusOfRequest($requestId,$pickupStatus)
	{
		DB::table('fareGenerated')->where('requestId',$requestId)->update(['status'=>'0','updatedAt'=>time()]);
		DB::table('userDestinationData')->where('requestId',$requestId)->update(['status'=>'0','updatedAt'=>time()]);
        if($pickupStatus=='4')
        {
		    DB::table('requests')->where('id',$requestId)->update(['pickupStatus'=>'4','status'=>'0','paymentStatus'=>'1','updatedAt'=>time()]);
        }
else if($pickupStatus=='5')
{
	DB::table('requests')->where('id',$requestId)->update(['pickupStatus'=>'5','status'=>'0','paymentStatus'=>'0','updatedAt'=>time()]);
}
		
		//write code to check if payment is through cc ...
		return 1;
	}
	
	protected function endTripTime($requestId)
	{
		DB::table('userDestinationData')->where('requestId',$requestId)->update(['updatedAt'=>time(),'tripEndedAt'=>time()]);
		return 1;
	}
	
	protected function updateRequestStatus($requestId,$pickupStatus)
	{
		DB::table('requests')->where('id',$requestId)->update(['pickupStatus'=>$pickupStatus,'updatedAt'=>time()]);
		return 1;
	}
	
	protected function requestHistory($driverId)
	{
		$data=DB::table('requests')->select('requests.*','users.firstName as firstName','users.lastName as lastName','userDestinationData.sourceLocation as sourceLocation','userDestinationData.dropLocation as dropLocation','fareGenerated.fare as fare')->join('userDestinationData','userDestinationData.requestId','=','requests.id')->join('fareGenerated','fareGenerated.requestId','=','requests.id')->join('users','users.id','=','requests.userId')->where('requests.driverId',$driverId)->where('requests.status','0')->where('requests.pickupStatus','!=','5')->orderBy('requests.id','DESC')->get();
		$dat=count($data)>0 ? $data : '0';
		return $dat;
	}
	
	

	

}