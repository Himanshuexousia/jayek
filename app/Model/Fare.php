<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class Fare extends Model
{
   protected $table = 'fareGenerated';
	 protected $primaryKey = 'id';
	 public $timestamps = false;

	  protected $fillable = array(
        'requestId',
		'userId',
		'driverId',
		'createdAt',
		'updatedAt'
    );
	

}