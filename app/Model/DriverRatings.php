<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Hash;

class DriverRatings extends Model
{
	 protected $table = 'driverratings';
	 protected $primaryKey = 'id';
	 public $timestamps = false;

	  protected $fillable = array(
        'userId',
		'driverId',
		'ratings',
		'reviews',
		'status',
		'createdAt',
		'updatedAt'
    );
	
}