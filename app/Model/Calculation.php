<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class Calculation extends Model
{
    //
	protected function calculateDistance($latitude,$longitude,$latitude1,$longitude1)
	{
		$theta=$longitude - $longitude1;
           $dist=sin(deg2rad($latitude)) * sin(deg2rad($latitude1)) +  cos(deg2rad($latitude)) * cos(deg2rad($latitude1)) * cos(deg2rad($theta));
           $dist=acos($dist);
           $dist=rad2deg($dist);
           $miles=$dist * 60 * 1.1515;
           $dist=$miles * 1.609344;   
		   return $dist;
	}
	
	protected function calculateFare($truckId,$distance)
	{
		$fare=DB::table('truckType')->select('fare')->where('id',$truckId)->first();
		$calculateFare=$fare->fare*$distance;
		return $calculateFare;
	}
	
	
	protected function fareSlip($requestId)
	{
		$data=DB::table('fareGenerated')->where('requestId',$requestId)->first();
		$dat=DB::table('userDestinationData')->where('requestId',$requestId)->first();
		$distance=$this->calculateDistance($dat->sourceLatitude,$dat->sourceLongitude,$dat->dropLatitude,$dat->dropLongitude);
		$tripTiming=$this->calculateTripTiming($dat->tripStartedAt,$dat->tripEndedAt);
		$json=array('fare'=>$data->fare,'distance'=>(string)round($distance,2),'tripTime'=>(string)$tripTiming,'userId'=>intval($data->userId));
		return $json;
	}
	protected function calculateTripTiming($start,$end)
	{
		//dd($start);
		$tripStart=date('H:i:s',$start);
		// dd($tripStart);
		$tripEnd=date('H:i:s',$end);
			// dd($tripEnd);
		$tripTiming=$end-$start;
		// dd(date('H:i:s',$tripTiming));
		return $tripTiming;

	}
	
	
	protected function getAverageRating($driverId)
	{
		$data=DB::table('driverratings')->where('driverId',$driverId)->get();
		$count=count($data);
		if(count($data)>0)
		{
			foreach ($data as $dat)
			{
				$var[]=$dat->ratings;
			}
			$sum=array_sum($var);
			$average=$sum/$count;
			return $average;
			
		}
		else
		{
			return 0;
		}
	}	
}