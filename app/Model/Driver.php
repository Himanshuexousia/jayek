<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
	protected $table='drivers';
	protected $primaryKey = 'id';
	public $timestamps=false;
	
	 protected function getDriverData($phone)
	 {
		 $get=Driver::where('phoneNumber','=',$phone)->orwhere('userName',$phone)->orWhere('email',$phone)->first();
		 return $get;
	 }
	 
	 protected function updateDriverLoginData($phone,$sessionTime,$authenticationToken,$loginPlatform,$notificationToken)
	 {
		$update=Driver::where('phoneNumber',$phone)->update(['sessionTime'=>$sessionTime,'authorizationToken'=>$authenticationToken,'loginPlatform'=>$loginPlatform,"notificationToken"=>$notificationToken,"updatedAt"=>time()]);
		return 1;
	 }
	
	
	
	protected function nearbyData($truckType,$latitude,$longitude,$distance)
	{
		
		$get=DB::select(DB::raw('select drivers.id As driverId,phoneNumber,firstName,lastName,email,truckDataId,driverLocation.latitude,driverLocation.longitude,vehicleNumber,truckData.id as truckId,truckData.truckTypeId AS truckTypeId,Round(3959 * acos( cos( radians(' .$latitude . ') ) * cos( radians( driverLocation.latitude ) ) * cos( radians( driverLocation.longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(driverLocation.latitude) ) ),2) AS distance from drivers JOIN driverLocation ON drivers.id=driverLocation.driverId JOIN truckData ON drivers.truckDataId=truckData.id where truckData.truckTypeId='.$truckType.' HAVING distance <'.$distance.' '));
		
		return $get;
	}
	
	protected function updateDriverLatLang($driverId,$latitude,$longitude)
	{
		$data=DB::table('driverLocation')->where('driverId',$driverId)->where('status','1')->first();
		if(count($data)>0)
		{
		    $update=DB::table('driverLocation')->where('driverId',$driverId)->where('status','1')->update(['oldLatitude'=>$data->latitude,'oldLongitude'=>$data->longitude,'latitude'=>$latitude,'longitude'=>$longitude,'updatedAt'=>time()]);
		    return 1;
		}
		else
		{
			$add=DB::table('driverLocation')->insert(['driverId',$driverId,'latitude'=>$latitude,'longitude'=>$longitude,'oldLatitude'=>'','oldLongitude'=>'','status'=>'1','createdAt'=>time(),updatedAt=>time()]);
			return 1;
		}
	}
	
	protected function getUserDataFromId($userId,$requestId)
	{
		$dat=DB::table('users')->select('users.*','userDestinationData.*','requests.createdAt as createat')->where('users.id',$userId)->join('userDestinationData','userDestinationData.userId','=','users.id')->join('requests','requests.userId','=','users.id')->where('requests.id',$requestId)->where('userDestinationData.requestId',$requestId)->first();
	
		$data=count($dat)>0 ? $dat : '0';
		return $data;
	}	
	
	protected function getDriverDataFromId($driverId,$requestId)
	{
		$dat=DB::table('drivers')->where('id',$driverId)->first();
		$data=count($dat)>0 ? $dat : '0';
		return $data;
	}
	protected function getRequestId($userId)
	{
		$data=DB::table('requests')->select('id')->where('userId',$userId)->orderBy('id','DESC')->first();
		
		return $data->id;
	}
	protected function getUserFromRequestId($requestId)
	{
		$data=DB::table('requests')->where('requests.id',$requestId)->join('users','users.id','=','requests.userId')->first();
		return $data;
	}
	protected function getDriverDataFromRequest($requestId)
	{
		$data=DB::table('requests')->where('requests.id',$requestId)->join('drivers','drivers.id','=','requests.driverId')->join('driverLocation','driverLocation.driverId','=','drivers.id')->join('truckData','truckData.id','=','drivers.truckDataId')->first();
		return $data;
	}
	
    // protected function getDriverLocationFromRequest($requestId)
	// {
		// $data=$data=DB::table('requests')->select('drivers.firstName as driverFirstName','drivers.lastName as driverLastName','drivers.phoneNumber as driverPhoneNumber','truckData.vehicleNumber as vehicleNumber','truckData.truckTypeId as truckTypeId','driverLocation.latitude as currentLatitude','driverLocation.longitude as currentLongitude','driverLocation.')->where('requests.id',$requestId)->join('drivers','drivers.id','=','requests.driverId')->join('driverLocation','driverLocation.driverId','=','drivers.id')->join('truckData','truckData.id','=','drivers.truckDataId')->first();
	// }
}
