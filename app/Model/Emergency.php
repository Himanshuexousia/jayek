<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Hash;

class Emergency extends Model
{
	 protected $table = 'emergencyContacts';
	 protected $primaryKey = 'id';
	 public $timestamps = false;

	  protected $fillable = array(
        'userId',
		'emergencyContact',
		'emergencyName',
		'priority',
		'status',
		'createdAt',
		'updatedAt'
    );
	
}
