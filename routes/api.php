<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
    // return $request->user();
// });
Route::get('slider', [ 'as' => 'slider','uses' =>'Webservice@slider']); 
Route::post('otp', [ 'as' => 'otp','uses' =>'Webservice@otp']); 
Route::get('truckType', [ 'as' => 'truckType','uses' =>'Webservice@truckType']); 
Route::get('demo', [ 'as' => 'demo','uses' =>'Webservice@demo']); 
Route::post('registerUser', [ 'as' => 'registerUser','uses' =>'Userregister@registerUser']); 
Route::post('checkUser', [ 'as' => 'checkUser','uses' =>'Userlogin@checkUser']);
Route::post('checkDriver', [ 'as' => 'checkDriver','uses' =>'DriverLogin@checkDriver']);
Route::get('versionType', [ 'as' => 'versionType','uses' =>'Webservice@versionType']); 
Route::post('calculateFare', [ 'as' => 'calculateFare','uses' =>'Calculatefare@calculateFare']); 
Route::post('checkPhoneNumber', [ 'as' => 'checkPhoneNumber','uses' =>'Webservice@checkPhoneNumber']); 
Route::post('nearbyDrivers', [ 'as' => 'nearbyDrivers','uses' =>'Nearby@nearbyDrivers']); 
Route::post('updateDriverLocation', [ 'as' => 'updateDriverLocation','uses' =>'LocationUpdate@updateDriverLocation']); 
Route::post('generateRequest', [ 'as' => 'generateRequest','uses' =>'RequestGenerate@generateRequest']); 
Route::post('driverRequest', [ 'as' => 'driverRequest','uses' =>'RequestGenerate@driverRequest']); 
Route::post('changeStatus', [ 'as' => 'changeStatus','uses' =>'RequestGenerate@changePickupStatus']);
Route::post('driverHistory', [ 'as' => 'driverHistory','uses' =>'RequestGenerate@driverHistory']);
Route::post('fareSlipData', [ 'as' => 'fareSlipData','uses' =>'RequestGenerate@fareSlipData']);
Route::post('driverData', [ 'as' => 'driverData','uses' =>'RequestGenerate@driverData']);
Route::post('driverLocation', [ 'as' => 'driverLocation','uses' =>'RequestGenerate@driverLocation']);
Route::post('editProfile', [ 'as' => 'editProfile','uses' =>'EditProfile@editProfile']);
Route::post('changePassword', [ 'as' => 'changePassword','uses' =>'EditProfile@changePassword']);
Route::post('userRides', [ 'as' => 'userRides','uses' =>'Userlogin@userRides']);
Route::post('ratings', [ 'as' => 'ratings','uses' =>'UploadRating@uploadRatings']);
Route::post('emergencyContact', [ 'as' => 'emergencyContact','uses' =>'EmergencyContacts@emergencyContacts']);
Route::post('emergencyContactList', [ 'as' => 'emergencyContactList','uses' =>'EmergencyContacts@getEmegencyContactsFull']);
